/**
 * **********************************************************************
 *
 * <p>DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * <p>Use is subject to license terms.
 *
 * <p>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0. You can also obtain a copy of the License at
 * http://odftoolkit.org/docs/license.txt
 *
 * <p>Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 *
 * <p>See the License for the specific language governing permissions and limitations under the
 * License.
 *
 * <p>**********************************************************
 */
package org.coverrest;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.Attributes2Impl;
import org.xml.sax.helpers.DefaultHandler;

/**
 * The base functionality of this class is to get the difference of two Coverage files (in the Cobertura XML coverage file format) expressed in source code lines as a new Cobertura XML coverage format.

  1) With a single Coverage Cobertura XML file as parameter it strips all the uncovered lines away to minimize follow-up processing time, disc space and reducing noise when viewing the file, as only the covered lines are of interest in our follow-up processing.
  2) With two Coverage Cobertura XML file as parameters the difference of covered lines are being calculated. Like in an arithmetic calculation the first parameter is the minuend (which coverage will be reduced) by the second parameter the subtrahend (see https://en.wikipedia.org/wiki/Subtraction#Notation_and_terminology).

 */
public class CoberturaXMLHandler extends DefaultHandler {

  public CoberturaXMLHandler(Coverage coverage) {
    mCov = coverage;
  }

  public CoberturaXMLHandler(Coverage coverageMinuend, Coverage coverageSubtrahend) {
    mCov = coverageMinuend;
    mCovSubtrahend = coverageSubtrahend;
  }

  Coverage mCov = null;
  Coverage mCovSubtrahend = null;

  private static final String TEST_INPUT_DIR_NAME =
      "src"
          + File.separator
          + "test"
          + File.separator
          + "resources"
          + File.separator
          + "test-input"
          + File.separator
          + "feature"
          + File.separator
          + "coverage"
          + File.separator;
  // e.g. within odftoolkit/odfdom/target/test-classes/test-input/feature/coverage

  /* empty output
    private static final String COBERTURA_FILENAME__MINUEND =
      TEST_INPUT_DIR_NAME + "cobertura_text_italic_stripped-indent.cov";

    private static final String COBERTURA_FILENAME__SUBTRAHEND =
      TEST_INPUT_DIR_NAME + "cobertura_bold__indent_stripped-indent.cov";
  private static final String COBERTURA_FILENAME__MINUEND =
    TEST_INPUT_DIR_NAME + "cobertura_bold__indent_stripped-indent.cov";

  private static final String COBERTURA_FILENAME__SUBTRAHEND =
    TEST_INPUT_DIR_NAME + "cobertura_text_italic_stripped-indent.cov";
  */

  /** LibreOffice stress test coverage files */
  private static final String COBERTURA_FILENAME__MINUEND = "coverage_loadBoldTextODT.cov";

  private static final String COBERTURA_FILENAME__SUBTRAHEND = "coverage_loadPlainODT.cov";

  // mStrippedWriter will be filled twice
  StreamWriter mStrippedWriter = null;

  StreamWriter mStrippedWriter_Diff = null;
  // keeping all information from start elements
  // until it is certain the XML should be written
  Deque<ElementInfo> mStartElementStack = new ArrayDeque<ElementInfo>();
  Deque<ElementInfo> mStartElementStack_Diff = new ArrayDeque<ElementInfo>();
  boolean mIsCoveredCondition = false;
  boolean mIsCoveredCondition_Diff = false;

  /** Some Coverage implementations are writing coverage lines within methods and aside of methods - we are ignoring the duplication within methods */
  boolean isWithinMethods = false;
  Locator mLocator;

  /** With the DocumentLocator line numbers will be received during errors */
  @Override
  public void setDocumentLocator(Locator locator) {
    mLocator = locator;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    if (qName.equals("line")
        || qName.equals("lines")
        || qName.equals("method")
        || qName.equals("methods")
        || qName.equals("class")
        || qName.equals("classes")
        || qName.equals("package")
        || qName.equals("packages")) {

      // stack as the elements are only written, if at least one line has a hit > 1 (otherwise
      // elements are neglected)
      mStartElementStack.push(new ElementInfo(uri, localName, qName, attributes));
      if (mCovSubtrahend != null) {
        mStartElementStack_Diff.push(new ElementInfo(uri, localName, qName, attributes));
      }
      if (qName.equals("class")) {
        String className = getAttributeValue(attributes, "name");
        String fileName = getAttributeValue(attributes, "filename");
        if (className != null && !className.isBlank() || fileName != null && !fileName.isBlank()) {
          try {
            // sets all dependant state changes
            mCov.newClassCoverage(className, fileName);
            if (mCovSubtrahend != null) {
              mCovSubtrahend.updateClassId(className, fileName);
            }
          } catch (Exception ex) {
            Logger.getLogger(CoberturaXMLHandler.class.getName())
                .log(
                    Level.SEVERE,
                    "The input cobertura file has unexpected split class coverage!",
                    ex);
          }
        }
      } if(qName.equals("methods")){
        // there will be no line evaluation within lines to normalize Coverage reports
        // GCovr has empty method(s), while JaCoCo duplicates lines within and after method(s)
        isWithinMethods = true;
      } else if (qName.equals("line") && !isWithinMethods) {
        String hits = getAttributeValue(attributes, "hits");
        int hitCount = Integer.parseInt(hits);
        if (hitCount > 0) {
          boolean hasConditionCoverage = hasConditionCoverage(attributes);
          if (hasConditionCoverage) {
            mIsCoveredCondition = true;
          }
          String number = getAttributeValue(attributes, "number");
          int lineNo = Integer.parseInt(number);
          // adding the new line coverage
          mCov.addLineCoverage(lineNo, hitCount, mLocator);
          flushStartElements(mStartElementStack, mStrippedWriter);
          if (mCovSubtrahend != null) {
            // make sure that the subtrahend lineNo is not less the one of the minuend
            mCovSubtrahend.compareCoverageByLineNoShift(lineNo, mLocator);
            if (mCovSubtrahend.mCurrentClass_CoveredLines == null
                || mCovSubtrahend.mLineNo == 0
                || mCovSubtrahend.mLineNo != lineNo) {
              if (hasConditionCoverage) {
                mIsCoveredCondition_Diff = true;
              }
              // writing start elements
              // saving fact/state that a start-element was already written into stream in the stack
              flushStartElements(mStartElementStack_Diff, mStrippedWriter_Diff);
              /*
              // This line hit difference between minuend and subtrahend is not helpful for the code cognita
              } else {
                if ((mCovSubtrahend.mLineNo == lineNo && hitCount > mCovSubtrahend.mHitCount)) {
                  // only a feature of the current parsed file mCov
                  int index = attributes.getIndex("hits");
                  mStartElementStack_Diff.pop(); // remove wrong hit attribute
                  Attributes2Impl updatedAttributes = new Attributes2Impl(attributes);
                  updatedAttributes.setValue(
                      index, Integer.toString(hitCount - mCovSubtrahend.mHitCount));
                  mStartElementStack_Diff.push(
                      new ElementInfo(uri, localName, qName, updatedAttributes));
                  if (hasConditionCoverage) {
                    mIsCoveredCondition_Diff = true;
                  }
                  flushStartElements(mStartElementStack_Diff, mStrippedWriter_Diff);
                } */
            }
          }
        }
      }
    } else {
      /* NOTE: The condition element always follows a line element
      and will be written if the prior line was written (hit > 0) */
      if (qName.equals("condition") || qName.equals("conditions")) {
        if (mIsCoveredCondition) {
          mStrippedWriter.writeStartElement(uri, localName, qName, attributes);
        }
        if (mIsCoveredCondition_Diff) {
          mStrippedWriter_Diff.writeStartElement(uri, localName, qName, attributes);
        }
      } else if (qName.equals("coverage")) {
        mStrippedWriter.writeStartElementWithoutAttribute(
            uri, localName, qName, attributes, "timestamp");
        if (mCovSubtrahend != null) {
          mStrippedWriter_Diff.writeStartElementWithoutAttribute(
              uri, localName, qName, attributes, "timestamp");
        }
      } else { // all other elements (not mentioned earlier) will be written out
        mStrippedWriter.writeStartElement(uri, localName, qName, attributes);
        if (mCovSubtrahend != null) {
          mStrippedWriter_Diff.writeStartElement(uri, localName, qName, attributes);
        }
      }
    }
  }
  /*
  private void adjustSubtrahendLine(int lineNo){
    boolean hasMultipleHits = false;
    int slineNo = mCovSubtrahend.mLineNo;
    if(slineNo < 0){
      slineNo*=-1;
    }
    while (slineNo != 0 || slineNo < lineNo) {
      updateLineAndHitNo()
    }
  }
  */

  private boolean hasConditionCoverage(Attributes attributes) {
    String conditionCoverage = attributes.getValue("condition-coverage");
    return (conditionCoverage != null && !conditionCoverage.isBlank());
  }

  private String getAttributeValue(Attributes attributes, String attrName) {
    String attrValue = attributes.getValue(attrName);
    if (attrValue == null || attrValue.isBlank()) {
      System.err.println(
          "getAttributeValue: Line"
              + mLocator.getLineNumber()
              + " Column"
              + mLocator.getColumnNumber()
              + ": CoverageXML "
              + attrName
              + " is empty or null:'"
              + attrValue
              + "'!");
      // } else {
      // System.out.println("Line" + mLocator.getLineNumber() + "Column" +
      // mLocator.getColumnNumber() + ": " + attrName + ":'" + attrValue + "'!");
    }
    return attrValue;
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equals("line")
        || qName.equals("lines")
        || qName.equals("method")
        || qName.equals("methods")
        || qName.equals("class")
        || qName.equals("classes")
        || qName.equals("package")
        || qName.equals("packages")) {
      ElementInfo elementInfo = mStartElementStack.pop();
      if (elementInfo.mIsStartElementWritten) {
        mStrippedWriter.writeEndElement();
      }
      if (mCovSubtrahend != null) {
        ElementInfo elementInfo_Diff = mStartElementStack_Diff.pop();
        if (elementInfo_Diff.mIsStartElementWritten) {
          mStrippedWriter_Diff.writeEndElement();
        }
      }
      if (qName.equals("class")) {
        mCov.updateClassId(null, null);
        if (mCovSubtrahend != null) {
          mCovSubtrahend.updateClassId(null, null);
        }
      }else if(qName.equals("methods")){
        isWithinMethods = false;
      }

    } else if (qName.equals("condition") || qName.equals("conditions")) {
      if (mIsCoveredCondition) {
        mStrippedWriter.writeEndElement();
        if (qName.equals("conditions")) {
          mIsCoveredCondition = false;
        }
      }
      if (mCovSubtrahend != null) {
        if (this.mIsCoveredCondition_Diff) {
          mStrippedWriter_Diff.writeEndElement();
          if (qName.equals("conditions")) {
            mIsCoveredCondition_Diff = false;
          }
        }
      }
    } else { // any other element will be written out
      // assumed not being in the descendant line of "line"
      // System.out.println("WRITING BASIC END:" + qName);
      mStrippedWriter.writeEndElement();
      if (mCovSubtrahend != null) {
        mStrippedWriter_Diff.writeEndElement();
      }
    }
  }

  public void startDocument() {
    mStrippedWriter = new StreamWriter(mCov.mOutputCoberturaXmlFile_stripped);
    if (mCovSubtrahend != null) {
      // initialize the two feature output streams
      mStrippedWriter_Diff = new StreamWriter(mCov.mOutputCoberturaXmlFile_Diff);
    }
  }

  public void endDocument() {
    mStrippedWriter.flushAndCloseWriter();
    if (mCovSubtrahend != null) {
      mStrippedWriter_Diff.flushAndCloseWriter();
    }
  }

  public static void main(String[] params) {
    String coberturaInputFileName_FeatureA = null;
    String coberturaInputFileName_FeatureB = null;
    if (params.length == 0 || params.length > 2) {
      printErrorManual();
    } else {
        if (params.length == 1) {
        coberturaInputFileName_FeatureA = params[0];
        if (coberturaInputFileName_FeatureA == null || coberturaInputFileName_FeatureA.isBlank()) {
          coberturaInputFileName_FeatureA = COBERTURA_FILENAME__MINUEND;
        }
      } else if (params.length == 2) {
        coberturaInputFileName_FeatureA = params[0];
        coberturaInputFileName_FeatureB = params[1];
        if (coberturaInputFileName_FeatureA == null
            || coberturaInputFileName_FeatureA.isBlank()
            || coberturaInputFileName_FeatureB == null
            || coberturaInputFileName_FeatureB.isBlank()) {
          coberturaInputFileName_FeatureA = COBERTURA_FILENAME__MINUEND;
          coberturaInputFileName_FeatureB = COBERTURA_FILENAME__SUBTRAHEND;
        }
      }
      try {
        if (coberturaInputFileName_FeatureB == null) {
          readCoberturaFile(coberturaInputFileName_FeatureA);
        } else {
          Coverage coverage_FeatureB = readCoberturaFile(coberturaInputFileName_FeatureB);
          diffCoberturaFiles(coberturaInputFileName_FeatureA, coverage_FeatureB);
        }
      } catch (Exception ex) {
        Logger.getLogger(CoberturaXMLHandler.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  private static void printErrorManual() {
    System.err.println(
        "USAGES:\n"
            + "\tINPUT:\n"
            + "\t\tCalled with only 1 PARAMETER\n"
            + "\t\t   Relative path from working directory to the Cobertura Coverage XML file that will be stripped from no coverage lines.\n\n"
            + "\t\tCalled with a 2nd PARAMETER\n"
            + "\t\t   The 2nd is the relative path to the Cobertura Coverage XML file (relative to user's directory).\n"
            + "\t\t   The 1st Cobertura XML file will be the Minuend (usually with the bigger feature set)\n"
            + "\t\t   and the second the Subtrahend: Minuend - Subtrahend = Difference\n"
            + "\t\t   In other words, the coverage of the second will be subtracted from the first.\n"
            + "\tOUTPUT:\n"
            + "\t   There will be a coverage output file for each input file reduced to 'hit lines' each file with the trunc suffix '_stripped_'\n"
            + "\t   In case of second parameter, there will be also a file showing the coverage difference of both coverages: 'the feature difference'.\n"
            + "\t   The feature diff file's trunc name ends with '_diff' and is saved to the users's directory!\n\n");
  }

  public static Coverage diffCoberturaFiles(
      String inputCoberturaFileName, Coverage coverageSubtrahend) throws Exception {
    System.err.println("\n\n ****************** Starting with DIFF!");
    SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
    Coverage coverage = new Coverage(inputCoberturaFileName);
    parser.parse(
        coverage.mInputCoberturaXmlFile, new CoberturaXMLHandler(coverage, coverageSubtrahend));
    return coverage;
  }

  public static Coverage readCoberturaFile(String inputCoberturaFileName) throws Exception {
    SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
    Coverage coverage = new Coverage(inputCoberturaFileName);
    parser.parse(coverage.mInputCoberturaXmlFile, new CoberturaXMLHandler(coverage));
    return coverage;
  }

  /** As soon a line with coverage was found all ancestor start elements will be written out */
  private void flushStartElements(Deque<ElementInfo> startElementStack, StreamWriter streamWriter) {
    Iterator<ElementInfo> it = startElementStack.descendingIterator();
    while (it.hasNext()) {
      ElementInfo elementInfo = it.next();
      if (!elementInfo.mIsStartElementWritten) {
        streamWriter.writeStartElement(
            elementInfo.uri, elementInfo.localName, elementInfo.qName, elementInfo.attributes);
        // System.out.println("Writing StartElement with qName:" + elementInfo.qName);
        elementInfo.mIsStartElementWritten = true;
      }
    }
  }

  static class StreamWriter {

    XMLStreamWriter mXsw = null;

    public StreamWriter(File outputFile) {

      XMLOutputFactory xof = XMLOutputFactory.newInstance();
      try {
        // make sure the output directories are being created

        outputFile.getParentFile().mkdirs();
        mXsw = xof.createXMLStreamWriter(new FileWriter(outputFile.getAbsolutePath()));
        mXsw.writeStartDocument();

      } catch (Exception e) {
        System.err.println("Unable to write the file: " + e.getMessage());
      }
    }

    public void writeStartElement(
        String uri, String localName, String qName, Attributes attributes) {
      if (qName == null || qName.isBlank()) {
        System.err.println("ERROR: Non existent qname: " + qName);
      }
      try {
        mXsw.writeStartElement(qName);
        for (int i = 0; i < attributes.getLength(); i++) {
          mXsw.writeAttribute(attributes.getQName(i), attributes.getValue(i));
        }
      } catch (XMLStreamException ex) {
        Logger.getLogger(CoberturaXMLHandler.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

    public void writeStartElementWithoutAttribute(
        String uri,
        String localName,
        String qName,
        Attributes attributes,
        String qNameOfattributeToOmit) {
      if (qName == null || qName.isBlank()) {
        System.err.println("ERROR: Non existent qname: " + qName);
      }
      try {
        mXsw.writeStartElement(qName);
        for (int i = 0; i < attributes.getLength(); i++) {
          if (!attributes.getQName(i).equals(qNameOfattributeToOmit)) {
            mXsw.writeAttribute(attributes.getQName(i), attributes.getValue(i));
          }
        }
      } catch (XMLStreamException ex) {
        Logger.getLogger(CoberturaXMLHandler.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

    public void writeEndElement() {
      try {
        mXsw.writeEndElement();
      } catch (XMLStreamException ex) {
        Logger.getLogger(CoberturaXMLHandler.class.getName()).log(Level.SEVERE, null, ex);
        try {
          if (mXsw != null) {
            mXsw.close();
            mXsw = null;
          }
        } catch (Exception e) {
          System.err.println("Unable to close the file: " + e.getMessage());
        }
      }
    }

    public void flushAndCloseWriter() {
      try {
        mXsw.writeEndDocument();
        mXsw.flush();
      } catch (Exception e) {
        System.err.println("Unable to write the file: " + e.getMessage());
      } finally {
        try {
          if (mXsw != null) {
            mXsw.close();
            mXsw = null;
          }
        } catch (Exception e) {
          System.err.println("Unable to close the file: " + e.getMessage());
        }
      }
    }
  }

  static class Coverage {

    public File mInputCoberturaXmlFile;
    public File mOutputCoberturaXmlFile_stripped;
    public File mOutputCoberturaXmlFile_Diff;
    public String mCurrentClassId;
    public Integer mLineNo = 0;
    public int mLineIndex = -1;
    public Integer mHitCount = 0;

    public List<Integer> mCurrentClass_CoveredLines;
    private List<Integer> mCurrentClass_LineHits;
    private Map<String, List<Integer>> mClassCoveragesLines;
    /** here the negativ lineNo indicates a hit other than default 1 */
    private Map<String, List<Integer>> mClassLineHits;

    private Map<Integer, Integer> mLineHits;
    private Iterator<Integer> mLineIterator = null;

    public Coverage(String inputCoberturaFileName) {
      initializeInputOutputFiles(inputCoberturaFileName);

      mClassCoveragesLines = new HashMap<String, List<Integer>>();
      mClassLineHits = new HashMap<String, List<Integer>>();
      mLineHits = new HashMap<Integer, Integer>();
    }

    /** @return an ordered list of line numbers */
    public List getClassCoverage(String className, String fileName) {
      return mClassCoveragesLines.get(className + "___" + fileName);
    }

    /** @return an empty list of line numbers */
    public void newClassCoverage(String className, String fileName) throws Exception {

      if (getClassCoverage(className, fileName) != null) {
        throw new Exception(
            "The input cobertura file has unexpected split class coverage of class: '"
                + className
                + " and file"
                + fileName
                + "'!");
      }
      mCurrentClassId = className + "___" + fileName;
      // collection of all upcoming lines (with hit > 0)
      mCurrentClass_CoveredLines = new LinkedList<>();
      mClassCoveragesLines.put(mCurrentClassId, mCurrentClass_CoveredLines);

      // collection of all hits > 1 <- indicated internally by negative line number
      mCurrentClass_LineHits = new LinkedList<>();
      mClassLineHits.put(mCurrentClassId, mCurrentClass_LineHits);
    }

    public void updateClassId(String className, String fileName) {
      mLineIndex = -1;
      mCurrentClassId = null;

      if (className != null && fileName != null) {
        mCurrentClassId = className + "___" + fileName;
        mCurrentClass_CoveredLines = mClassCoveragesLines.get(mCurrentClassId);
        mCurrentClass_LineHits = mClassLineHits.get(mCurrentClassId);
      } else if (className == null || fileName == null) {
        mCurrentClassId = null;
      } else {
        System.err.println(
            "ClassName '" + className + "' or fileName '" + fileName + "' should not be null!");
      }
    }

    // As the coverage of line numbers are always ordered in both files to be compared we use a pointer mLineIndex for quick comparison */
    /** @param lineNo the minuend's line no */
    public void compareCoverageByLineNoShift(int lineNo, Locator locator) {
      boolean hasLineChanged = false;
      // if there are any covered lines for this class
      if (mCurrentClass_CoveredLines != null) {
        // initialize line number
        // if a line index exist and its last line was not read yet
        if (mLineIndex <= 0 && mCurrentClass_CoveredLines.size() > (mLineIndex + 1)) {
          mLineIndex++;
          mLineNo = mCurrentClass_CoveredLines.get(mLineIndex);
          hasLineChanged = true;
        }
        // while the minuend's lineNo is higher than the subtrahend's
        while (lineNo > Math.abs(mLineNo) && mCurrentClass_CoveredLines.size() > (mLineIndex + 1)) {
          mLineIndex++;
          mLineNo = mCurrentClass_CoveredLines.get(mLineIndex);
          hasLineChanged = true;
        }
        // if there is a subtrahend's lineNo found and it is negativ, fetch hit
        if (hasLineChanged) {
          if (mLineNo < 0) {
            mLineNo *= -1;
            mHitCount = mLineHits.get(mLineNo);
          } else {
            mHitCount = 1;
          }
        }
      }
    }

    public void addLineCoverage(int lineNo, int hitCount, Locator locator) {
      if (mCurrentClass_CoveredLines == null) {
        System.err.println(
            "addLineCoverage: Line"
                + locator.getLineNumber()
                + "Column"
                + locator.getColumnNumber()
                + ": addLineCoverage "
                + mCurrentClassId
                + " is empty or null:'"
                + mCurrentClassId
                + "'!");
      }
      assert (hitCount > 0);
      if (hitCount > 1) {
        mLineHits.put(lineNo, hitCount);
        // most of the lineNo are 1 in the other rare case
        // we save the hitCount separately and indicate it by negative lineNo
        lineNo *= -1;
      }
      mCurrentClass_CoveredLines.add(lineNo);
    }

    private void initializeInputOutputFiles(String inputFileName) {
      mInputCoberturaXmlFile = getCoberturaXMLInputFile(inputFileName);
      mOutputCoberturaXmlFile_stripped = getCoberturaXMLOutputFile(inputFileName, "_stripped");
      mOutputCoberturaXmlFile_Diff = getCoberturaXMLOutputFile(inputFileName, "_diff");
    }

    private static File getCoberturaXMLInputFile(String coberturaXMLFileName) {
      return new File(System.getProperty("user.dir") + File.separator + coberturaXMLFileName);
    }

    private File getCoberturaXMLOutputFile(String coberturaXMLFileName, String newSuffix) {
      if (coberturaXMLFileName.contains(File.separator)) {
        coberturaXMLFileName =
            coberturaXMLFileName.substring(
                coberturaXMLFileName.lastIndexOf(File.separator), coberturaXMLFileName.length());
      }
      String strippedCoberturaFileName = null;
      String suffix = null;
      if (coberturaXMLFileName.contains(".cov.xml")) {
         suffix = coberturaXMLFileName.substring(coberturaXMLFileName.lastIndexOf(".cov.xml"));
        strippedCoberturaFileName = coberturaXMLFileName.replace(suffix, newSuffix + suffix);
      } else if (coberturaXMLFileName.contains(".")) {
         suffix = coberturaXMLFileName.substring(coberturaXMLFileName.lastIndexOf('.'));
        strippedCoberturaFileName = coberturaXMLFileName.replace(suffix, newSuffix + suffix);
      } else {
        strippedCoberturaFileName = coberturaXMLFileName.concat(newSuffix);
      }
      // odfdom/src/test/resources/test-reference/feature/cobertura
      return new File(
          System.getProperty("user.dir")
              + File.separator
//              + "src"
//              + File.separator
//              + "test"
//              + File.separator
//              + "resources"
//              + File.separator
//              + "test-reference"
//              + File.separator
//              + "feature"
//              + File.separator
//              + "coverage"
//              + File.separator
              + strippedCoberturaFileName);
    }
  }

  // all relevent information of an XML element (by startElement SAX event)
  static class ElementInfo {

    public String uri;
    public String localName;
    public String qName;
    public Attributes attributes;
    // if there is another subelement e.g. <method>
    // the already written parent <methods>
    // will not be written out again
    public boolean mIsStartElementWritten = false;

    public ElementInfo(String uri, String localName, String qName, Attributes attributes) {
      this.uri = uri;
      this.localName = localName;
      this.qName = qName;
      // Attributes2Impl will create a real copy, otherwise
      // all attributes are the same from the last element
      this.attributes = new Attributes2Impl(attributes);
    }
  }
}

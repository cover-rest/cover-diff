# Cover-Diff

## Goal of Cover-Diff

The main goal of this module Cover-Diff is offering a functionality accessible by Java API or CLI (Command Line Interface) API to create the DIFF of two Cobertura Coverage XML files.

The reason of the Cover-Diff had been explained in the [usage of Cover-Rest](https://gitlab.com/cover-rest/docs/-/blob/main/docs/coverrest-usage.md).

## Usage of Cover-Diff

 The base funcationality of this module is to get the difference of two Coverage files (in the Cobertura XML coverage file format) expressed in source code lines returned as a new Cobertura XML coverage format.

There are two use cases with two parameters or a single parameters, always providing the path to a Cobertura XML file:

1. With two Coverage Cobertura XML file as parameters the difference of covered lines are being calculaged. Like in an arithmetic calculationt he first parameter is the minued (which coverage will be reduced) by the second parameter the subtrahend (see https://en.wikipedia.org/wiki/Subtraction#Notation_and_terminology).

2. With a single Coverage Cobertura XML file as parameter all the uncovered lines are stripped away to minimize follow-up processing time, disc space and reducing noise when viewing the file, as only the covered lines are of interest in our follow-up processing. 
This becomes necessary, when the coverage result for a large code base as LibreOffice is shown in a viewer the explicit listing slows down this process painfully.

## Example Usage

As an example we added [our bash script diff-coverages.sh](https://gitlab.com/cover-rest/cover-diff/-/blob/master/diff-coverages.sh) that makes a coverage DIFF between two "new" coverage files from [the LibreOffice project](https://www.libreoffice.org).

1. Clone and build the Cover-Diff project to have the JAR in the ./target output directory
```bash
    git clone https://gitlab.com/cover-rest/cover-diff
    cd cover-diff
    mvn install
```

2. Make a diff between the two LibreOffice Copertura Coverage files in the root of the project.
The first is the minuend the second the subtrahend. Like: "minuend - subtrahend = difference"!
```bash
java -jar ./target/coverage-diff-1.0.1-jar-with-dependencies.jar \ 
    ./cobertura_loadBoldTextODT.cov.xml \
    ./cobertura_loadPlainTextODT.cov.xml
```
Each Cobertura file on the root is the result of a test loading a document in LibreOffice:

1. Loading a text file with text in bold. Becoming the minuend as we expect a higher feature set with higher code coverage.
2. Loading a text file with plain text. Becoming the subtrahend as we expect a lower feature set withh less code coverage.

**NOTE: Due to the size of the LibreOffice code, each coverage XML file is larger than 300 MB!!**
As stated earlier above, although the amount of covered code is little, the files created via JaCoCo & Cover2Cover have a XML line even for every line **not** being covered.
The new created stripped Coverage files only have a size of about 17 MB!

How to create coverage files is being shown for Java in [the ODFDOM Java project of our odftoolkit fork](https://gitlab.com/cover-rest/odftoolkit).
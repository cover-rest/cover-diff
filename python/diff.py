import logging

try:
    from restApi.parser import xml_parse
except ImportError:
    raise ImportError("Import failed, exec from cover-rest level or xml_parse can't import")


class CoverageDiff:
    minute: xml_parse.RootType = None  # for example Bold -> the feature coverage
    subtrahend: xml_parse.RootType = None  # for example without Bold -> the base coverage
    difference: xml_parse.RootType = None

    @classmethod
    def from_paths(cls, minute_path: str, subtrahend_path: str):
        self = cls()
        logging.info("loading minute...")
        self.minute = xml_parse.XmlParse.from_path(minute_path).coverage
        logging.info("loading subtrahend...")
        self.subtrahend = xml_parse.XmlParse.from_path(subtrahend_path).coverage
        logging.info("calculating diff...")
        self.difference = xml_parse.RootType(encoding=self.minute.encoding, doctype=self.minute.doctype,
                                             description=f"difference between {minute_path} and {subtrahend_path}",
                                             sources=[], packages=[])
        start_ = time.perf_counter()
        self.__diff()
        logging.debug(f"diff took: {time.perf_counter() - start_}s")
        logging.info("done")
        return self

    @classmethod
    def from_root_type(cls, minute: xml_parse.RootType, subtrahend: xml_parse.RootType):
        self = cls()
        logging.info("loading minute...")
        self.minute = minute
        logging.info("loading subtrahend...")
        self.subtrahend = subtrahend
        logging.info("calculating diff...")
        self.difference = xml_parse.RootType(description=f"difference between {minute.job_reference} and "
                                                         f"{subtrahend.job_reference}",
                                             sources=[], packages=[])
        start_ = time.perf_counter()
        self.__diff()
        logging.debug(f"diff took: {time.perf_counter() - start_}s")
        logging.info("done")
        return self

    def __diff(self):  # TODO check if coverage which is in subtrahend but not in minute
        for subtrahend_package in self.subtrahend.packages:
            if subtrahend_package.name not in [minute_package.name for minute_package in self.minute.packages]:
                self.difference.packages.append(subtrahend_package)
            else:
                self.difference.packages.append(
                    xml_parse.PackageType(name=subtrahend_package.name, classes=[])
                )

                minute_package = None
                for minute_package_loop in self.minute.packages:
                    if minute_package_loop.name == subtrahend_package.name:
                        minute_package = minute_package_loop
                minute_package_class_names = [minute_class.name for minute_class in minute_package.classes]

                for subtrahend_class in subtrahend_package.classes:
                    if subtrahend_class.name not in minute_package_class_names:
                        self.difference.packages[-1].classes.append(subtrahend_class)
                    else:
                        self.difference.packages[-1].classes.append(
                            xml_parse.ClassType(name=subtrahend_class.name, filename=subtrahend_class.filename,
                                                methods=[], lines=[])
                        )

                        minute_class = None
                        for minute_class_loop in minute_package.classes:
                            if minute_class_loop.name == subtrahend_class.name:
                                minute_class = minute_class_loop
                        minute_class_method_names = [minute_method.name for minute_method in minute_class.methods]

                        for subtrahend_method in subtrahend_class.methods:
                            if subtrahend_method.name not in minute_class_method_names:
                                self.difference.packages[-1].classes[-1].methods.append(subtrahend_method)
                            else:
                                self.difference.packages[-1].classes[-1].methods.append(
                                    xml_parse.MethodeType(name=subtrahend_method.name, filename=subtrahend_method.name,
                                                          lines=[])
                                )

                                minute_method = None
                                for minute_method_loop in minute_class.methods:
                                    if subtrahend_method.name == minute_method_loop.name:
                                        minute_method = minute_method_loop

                                self.difference.packages[-1].classes[-1].methods[-1].lines = self.__line_diff(
                                    subtrahend_method.lines, minute_method.lines)

                        self.difference.packages[-1].classes[-1].lines = self.__line_diff(subtrahend_class.lines,
                                                                                          minute_class.lines)

                        if len(self.difference.packages[-1].classes[-1].lines) == 0:
                            if len(self.difference.packages[-1].classes[-1].methods) != 0:
                                if len(self.difference.packages[-1].classes[-1].methods[-1].lines) == 0:
                                    self.difference.packages[-1].classes.pop()
                            else:
                                self.difference.packages[-1].classes.pop()
                        elif len(self.difference.packages[-1].classes[-1].methods) != 0:
                            if len(self.difference.packages[-1].classes[-1].methods[-1].lines) == 0:
                                self.difference.packages[-1].classes[-1].methods.pop()

                if len(self.difference.packages[-1].classes) == 0:
                    self.difference.packages.pop()

    def __line_diff(self, subtrahend_lines: list[dict], minute_lines: list[dict]) -> list[dict]:
        line_diff = []
        for subtrahend_line in subtrahend_lines:
            for minute_line in minute_lines:
                subtrahend_number = subtrahend_line.get("number")
                minute_number = minute_line.get("number")
                if subtrahend_number != minute_number:
                    continue
                if subtrahend_line == minute_line:
                    continue
                else:
                    line = {"number": subtrahend_line.get("number"), "hits": "0"}
                    subtrahend_hit = subtrahend_line.get("hits")
                    minute_hit = minute_line.get("hits")
                    if subtrahend_hit == minute_hit:
                        line["hits"] = subtrahend_hit
                    else:
                        line["hits"] = str(int(minute_hit) - int(subtrahend_hit))
                    if not subtrahend_line.get("branch") and not minute_line.get("branch"):
                        line_diff.append(line)
                        continue
                    line = self.__condition_diff(subtrahend_line, minute_line, line)
                    line_diff.append(line)
                    break
        return line_diff

    @staticmethod
    def __condition_diff(subtrahend_line: dict, minute_line: dict, return_line: dict) -> dict:
        subtrahend_branch = subtrahend_line.get("branch")
        minute_branch = minute_line.get("branch")
        if subtrahend_branch == minute_branch:
            return_line["branch"] = subtrahend_branch
        else:
            return_line["branch"] = minute_branch  # TODO find better way
        if subtrahend_line.get("condition_coverage") == minute_line.get("condition_coverage"):
            return_line["condition_coverage"] = subtrahend_line.get("condition_coverage")
        else:
            # TODO is this right?
            minu_split = minute_line.get("condition_coverage").split(" ")[-1].split("/")
            minu_hit = int(minu_split[0].replace("(", ""))
            minu_no_hit = int(minu_split[-1].replace(")", ""))
            subt_split = subtrahend_line.get("condition_coverage").split(" ")[-1].split("/")
            subt_hit = int(subt_split[0].replace("(", ""))
            subt_no_hit = int(subt_split[-1].replace(")", ""))
            if minu_no_hit == 0 and subt_no_hit == 0:  # TODO find better solution for zero division problem
                minu_no_hit = 0.0000000000000000000001
                subt_no_hit = 0.0000000000000000000001
            return_line[
                "condition_coverage"] = f"{int(minu_hit / minu_no_hit * 100) - int(subt_hit / subt_no_hit * 100)}% " \
                                        f"({minu_hit - subt_hit}/{minu_no_hit - subt_no_hit})"
        if subtrahend_line.get("conditions") == minute_line.get("conditions"):
            return_line["conditions"] = subtrahend_line.get("conditions")
        else:
            # TODO is this right?
            return_line["conditions"] = []
            for subtrahend_condition in subtrahend_line.get("conditions"):
                for minute_condition in minute_line.get("conditions"):
                    condition = {"number": "0", "type": "jump", "coverage": "0%"}
                    subt_number = subtrahend_condition.get("number")
                    minu_number = minute_condition.get("number")
                    subt_type = subtrahend_condition.get("type")
                    minu_type = minute_condition.get("type")
                    subt_percent = int(subtrahend_condition.get("coverage").replace("%", ""))
                    minu_percent = int(minute_condition.get("coverage").replace("%", ""))
                    if subt_number == minu_number:
                        condition["number"] = subt_number
                    else:
                        condition["number"] = minu_number  # TODO find better way
                    if subt_type == minu_type:
                        condition["type"] = subt_type
                    else:
                        condition["type"] = minu_type  # TODO find better way
                    if subt_percent == minu_percent:
                        condition["coverage"] = f"{subt_percent}%"
                    else:
                        condition["coverage"] = f"{minu_percent - subt_percent}%"
                    return_line["conditions"].append(condition)
        return return_line


if __name__ == "__main__":
    import time
    import os.path
    import argparse

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument("minute", help="minute for example Bold -> the feature coverage")
    arg_parser.add_argument("subtrahend", help="subtrahend for example without Bold -> the base coverage")
    arg_parser.add_argument("-o", "--output-file", help="output file name")

    args = arg_parser.parse_args()

    start = time.perf_counter()
    cover_diff = CoverageDiff.from_paths(args.minute, args.subtrahend)
    # cover_diff_backwards = CoverageDiff.from_root_type(cover_diff.subtrahend,
    #                                                    cover_diff.minute)
    # cover_diff_diff = CoverageDiff.from_root_type(cover_diff.difference, cover_diff_backwards.difference)
    # cover_diff_diff_backwards = CoverageDiff.from_root_type(cover_diff_backwards.difference, cover_diff.difference)
    logging.debug(f"exec took: {time.perf_counter() - start}s")
    xml_parser = xml_parse.XmlParse()
    xml_parser.coverage = cover_diff.difference
    if not args.output_file:
        path = "/".join(args.minute.split("/")[:-1])
        minu_basename = os.path.basename(args.minute).split(".")[0]
        subt_basename = os.path.basename(args.subtrahend).split(".")[0]
        args.output_file = os.path.join(path, f"{minu_basename}_diff_{subt_basename}.diff.cov.xml")
    logging.info(f"generated output path: {args.output_file}")
    xml_parser.to_xml(args.output_file)
    logging.info("completed")

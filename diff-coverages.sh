#!/bin/bash
@echo on
# -e causes the shell to exit if any subcommand or pipeline returns a non-zero status
# -v causes the shell to view each command
set -e -v


# !!! PLEASE UPDATE VARIABLES BELOW!!!

# ODT feature minuend document trunc name, here in src/test/resources/test-input/feature/
ODF_FEATURE_MINUENT__FILE_NAME__TRUNC="loadBoldTextODT"
# ODT feature subtrahend document trunc name, e.g src/test/resources/test-input/feature/
ODF_FEATURE_SUBTRAHEND__FILE_NAME__TRUNC="loadPlainTextODT"

# DO NOT CHANGE BELOW THE FOLLOWING LINE
# --------------------------------------------------------

# Showing Usage
java -jar ./target/coverage-diff-1.0.1-jar-with-dependencies.jar

# Strip the uncovered lines from the Cobertura Coverage XML, first argument the minuent the second the subtrahend!
java -jar ./target/coverage-diff-1.0.1-jar-with-dependencies.jar ./cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}.cov.xml ./cobertura_${ODF_FEATURE_SUBTRAHEND__FILE_NAME__TRUNC}.cov.xml

# copying the files of the user directory to later usage in regression tests into the source repository
cp cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_stripped.cov.xml src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_stripped.cov.xml
cp cobertura_${ODF_FEATURE_SUBTRAHEND__FILE_NAME__TRUNC}_stripped.cov.xml src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_SUBTRAHEND__FILE_NAME__TRUNC}_stripped.cov.xml
cp cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_diff.cov.xml src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_diff.cov.xml

# XML pretty-printing/indentation - xmllint comes with libxml2-utils: https://gitlab.gnome.org/GNOME/libxml2
xmllint --format src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_stripped.cov.xml > src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_stripped-indent.cov.xml
xmllint --format src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_stripped.cov.xml > src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FEATURE_MINUENT__FILE_NAME__TRUNC}_diff-indent.cov.xml
